import scala.io.{Source, BufferedSource}
import scala.util.{Try, Success, Failure}

case class Student(name: String, score: Float, grade: String)

def readRawData(filePath: String): Try[List[String]] =
    Try( Source.fromFile(filePath).getLines().toList )
 
def removeHeader(rawData: List[String]): Try[List[String]] = 
    Try( rawData.tail )
    
def splitByComman(rawData: List[String]): Try[List[Array[String]]] =
    Try( rawData.map{ _.split(",") } )

def convertToFloat(rawData: List[Array[String]]): Try[List[(String, Float, String)]] = 
    Try( rawData.map(line => (line(0), line(1).toFloat, line(2))) )  

def createListOfStudent(maybeData: List[(String, Float, String)])  = 
    Try(maybeData.map(e => Student(name = e._1, score = e._2, grade = e._3)))  

def calculateAverage(listOfStudent: List[Student]): Try[Double] = 
    Try( listOfStudent.map(_.score).sum / listOfStudent.length )

@main def start() = 
    val result: Try[Double] = Try("file.csv")
                        .flatMap(readRawData)
                        .flatMap(removeHeader)
                        .flatMap(splitByComman)
                        .flatMap(convertToFloat)
                        .flatMap(createListOfStudent)
                        .flatMap(calculateAverage)

    result match
        case Success(data)      => println(s"Total score of all student is $data")
        case Failure(exception) => println(s"Failed !!!! : ${exception.getMessage}")
    