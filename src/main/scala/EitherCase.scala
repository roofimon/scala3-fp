// Using Try for safe conversion
import scala.util.Try

def stringToInt(str: String): Option[Int] = Try(str.toInt).toOption
def divide(a: Int, b:Int): Option[Double] = Try((a/b).toDouble).toOption

def oentryPoint(): Unit = 
    val inputString = "200"
    
    val usingFlatMap = Some("200")
        .flatMap(a =>   stringToInt(a))
        .flatMap(b =>   divide(b, 2))
    
    val usingFor = for 
        intValue <- stringToInt(inputString)
        doubleValue <- divide(intValue, 2)
    yield doubleValue

    (usingFlatMap, usingFor) match
      case (Some(a), Some(b)) => println(s"Successfully converted to Int: $a and $b")
      case _                  => println("Failed to convert to Int")

