import scala.io.Source
import scala.util.Using
import scala.io.BufferedSource
import scala.util.Try
import scala.collection.Seq
import scala.util.Success

case class SStudent(name: String, score: Float, grade: String)


def readDataFromFile(filePath: String): Either[Exception, List[String]] =
    try
        val source = Source.fromFile(filePath)
        val csvDataLines = source.getLines().toList
        source.close()
        Right(csvDataLines)
    catch 
        case e: Exception => Left(e)
    
def fromCsvFormat(csvLine: List[String]): Either[Exception, List[Array[String]]] = 
    try
        //val Array(name, score, grade) = csvLine.split(",")
        Right(csvLine.map(line => line.split(",")))
        //Right((name, score, grade))
    catch
        case e: Exception => Left(e)
    
def convertScoreToFloat(data: List[Array[String]]): List[Either[NumberFormatException, (String, Float, String)]] = 
    for ( i <- data ) yield 
            val Array(name, score, grade) = i
            if (score.toFloat.isNaN) 
                Left(new NumberFormatException(s"Failed to parse score '$score' to float"))
                //throw new NumberFormatException(s"Failed to parse score '$score' to float")
            else 
                Right((name, score.toFloat, grade))

    

def createStudent(data: List[(String, Float, String)]): List[Either[Exception, Student]] = 
    for ( i <- data ) yield
        try
            Right(Student(name=i._1, score=i._2, grade=i._3))
        catch
            case e: Exception => Left(e)
def entryPoint() = 
    val ff = Right("file.csv")
                .flatMap((a: String) => readDataFromFile(a))
                .flatMap((b: List[String]) => fromCsvFormat(b))
                //.flatMap((c: List[Array[String]]) => convertScoreToFloat(c))
                //.flatMap(d => createStudent(d))
    // ff match
    //     case Right(data) => data.foreach(println)
    //     case Left(e) => println("xxxx")
