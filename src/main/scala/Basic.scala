// import scala.io.Source

// def readRawData(filePath:String) = 
//     try 
//         val source = Source.fromFile(filePath)
//         val lines = source.getLines().toList
//         source.close()
//         lines
//     catch
//         case e: Exception =>
//             println(s"An error occurred while reading the file: ${e.getMessage}")
//             List.empty[String]

// def removeHeader(rawData:List[String]): List[String] = 
//     if (rawData.length > 0) rawData.tail
//     else List.empty[String]

// def processData(rawData:List[String]): List[(String, String, String)] = 
//     rawData.map(  line =>
//         val Array(name, score, grade) = line.split(",")
//         (name, score, grade)
//     )

// def convertToFloat(rawData: List[(String, String, String)]): List[(String, Float, String)] =
//     for(line <- rawData) yield 
//         val (name, score, grade)  = line
//         (name, score.toFloat, grade)
    
// @main def startHere() =
//     // Specify the path to your CSV file
//     val filePath = "file.csv"
//     val pipeline = readRawData andThen removeHeader andThen processData andThen convertToFloat
//     val csvData = pipeline(filePath)
//     // Display the processed data
//     println("Name\tScore\tGrade")
//     csvData.foreach { case (name, score, grade) =>
//         println(s"$name\t$score\t$grade")
//     }
    
