val plus: Int => Int => Int = (a:Int) => (b:Int) => a + b
val multiply: Int => Int => Int = (a:Int) => (b: Int) => a * b

val addTwo = plus(2)
val multiplyByThree = multiply(3)
val pipeline = addTwo andThen multiplyByThree

// Test the composed function
val result: Int = pipeline(6)
println(result)

