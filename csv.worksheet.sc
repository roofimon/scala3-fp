  val mockCsv: String =
    """Name,Score,Grade
      |Alice,90,A
      |Bob,85,B
      |Charlie,78,C
      |David,92,A
      |Eva,88,B
      |Frank,79,C
      |Grace,95,A
      |""".stripMargin

case class Student(name: String, score: Int, grade: String)
// Parse CSV data and create Student objects
val students: List[Student] = mockCsv
            .stripMargin // Remove leading whitespace
            .split("\n") // Split by lines
            .tail // Skip the header
            .map { line =>
                val Array(name, score, grade) = line.split(",")
                Student(name, score.toInt, grade)
            }
            .toList

// Print the result or perform further processing
students.foreach(println)