val x = 9
if x < 0 then
  println("negative")
else if x == 0 then
  println("zero")
else
  println("positive")

val ints = List(1, 2, 3, 4, 5)

for i <- ints do println(i)  

val (name: String, score: Double, grade: String) = ("Ruf", 20.0, "A")
(name, score, grade) match
  case (name, score, grade) => println("XXXX")
  case _ => println("YYYY")

val some: Some[Int] = Some(200)
val listOf = List("apple", "banana", "orange")
listOf.map(_.length)
val eitherList = Right(List("apple", "banana", "orange"))
eitherList.map(_.length)
val data: List[Array[String]] = List(Array("a", "2", "b"), Array("a", "2", "b"))
val yyy = for ( i <- data ) yield {
    try
        val Array(name, score, grade) = i
        if (score.toFloat.isNaN) 
            throw new NumberFormatException(s"Failed to parse score '$score' to float")
        else 
            Right((name, score.toFloat, grade))
    catch
        case e: Exception => Left(e)
}

val scoreList = List(("a", 2, "b"), ("a", 2, "b")) 
val result = scoreList.foldLeft(0) {(sum, x) => x._2 + sum}

 val numRange = 1 to 5